import { Injectable } from '@angular/core';
// import { HEROES } from './heroes-mock';
import { Hero } from './Interfaces/Hero';
// Observable Hero Service
import { Observable, of } from 'rxjs';
// map, tap - server usage
import { map, tap } from 'rxjs/operators'
// Inject Message Service
import { MessageService } from './message.service';
// Heroes and HTTP, HttpClient, HttpHeaders
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HEROES } from './heroes-mock';

//  const httpOptions = {
//     headers: new HttpHeaders({
//       'Content-Type': 'application/json',
//       Authorization: 'my-auth-token'
//     })
//   };

@Injectable({ providedIn: 'root' })
export class HeroService {

  private heroesUrl = 'http://localhost:3000/heroes';

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  getHeroesWithHTTP() {
    return this.http.get<Hero[]>(this.heroesUrl)
  }

  getHero(id: number) {
    return this.http.get<Hero>(this.heroesUrl+'/'+id)
  }

  addHero(hero:Hero) {
    return this.http.post(this.heroesUrl, hero)
  }

  updateHero(hero:Hero) {
    return this.http.put(this.heroesUrl+'/'+hero.id, hero)
  }

  removeHero(hero:Hero) {
    return this.http.delete(this.heroesUrl+'/'+hero.id)
  }
}
