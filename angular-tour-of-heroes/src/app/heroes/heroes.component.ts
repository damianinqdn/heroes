import { Component, OnInit, Output } from '@angular/core';
// here import the new created interfaces
import { Hero } from '../Interfaces/Hero';
// import { HEROES } from '../heroes-mock';
import { HeroService } from '../hero.service';
import { MessageService } from '../message.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})


export class HeroesComponent implements OnInit {

  heroes$?: Observable<Hero[]> | undefined;

  // heroes?: Hero[];

  // Service Injection
  constructor(private heroService: HeroService,
    private messageService: MessageService) { }

  ngOnInit(): void {
    // this.getHeros();
    this.heroes$ = this.heroService.getHeroesWithHTTP();
  }

  removeHero(hero:Hero) {
    this.heroService.removeHero(hero).subscribe(
      (data: any) => {
        console.log(data);
        this.ngOnInit();
      }
    );
    this.ngOnInit();
    // this.heroes$ = this.heroService.getHeroesWithHTTP();
  }

}
