import { Component, OnInit } from '@angular/core';
import { Hero } from '../Interfaces/Hero';
import { HeroService } from '../hero.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  
  heroes: Hero[] = []

  // heroes$: Observable<Hero[]> | undefined;

  constructor(private heroService: HeroService) { }

  ngOnInit(): void {
    this.getHeroes()
  }
  getHeroes() {
    this.heroService.getHeroesWithHTTP()
      .subscribe(heroes => this.heroes = heroes.slice(1,5))
  }

  

}
