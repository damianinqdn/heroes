import { Component, Input, OnDestroy, OnInit } from '@angular/core';
// display Hero using routing
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Hero } from '../Interfaces/Hero';
import { HeroService } from '../hero.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit, OnDestroy {

  id?: number;
  hero?: Hero;
  heroSub$?: Subscription

  constructor(
    private route: ActivatedRoute,
    private heroService: HeroService,
    private location: Location
  ) { }

  ngOnDestroy(): void {
    this.heroSub$?.unsubscribe();
  }

  ngOnInit(): void {
    this.getHero();
  }
  getHero(): void {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.heroSub$ = this.heroService.getHero(this.id).subscribe(hero => {
      this.hero = hero;
      console.log(this.hero);
    })
  }

  updateHero(hero:Hero): void {
    this.heroService.updateHero(hero).subscribe();
  }

  addHero(hero:Hero) {
    const newHero:Hero = {
      id: hero.id,
      name: hero.name
    }
    this.heroService.addHero(newHero).subscribe();
    console.log(newHero.id, newHero.name);
    
  }

  goBack() {
    this.location.back();
  }

}
