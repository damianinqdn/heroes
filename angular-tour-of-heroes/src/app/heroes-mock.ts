import { Hero } from "./Interfaces/Hero";

export const HEROES: Hero[] = [
{id: 12, name: 'Patryk'},
{id: 13, name: 'Marcin'},
{id: 14, name: 'Darek'},
{id: 15, name: 'Zbyszek'},
{id: 16, name: 'Konrad'},
{id: 17, name: 'Sylwia'},
{id: 18, name: 'Kraolina'},
{id: 19, name: 'Małgosia'},
{id: 20, name: 'Robert'},
{id: 21, name: 'Stanisław'},
{id: 22, name: 'Radek'},


]